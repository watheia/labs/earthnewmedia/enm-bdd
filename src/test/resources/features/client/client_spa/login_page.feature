@client_spa
@login_page
@release:v0.1a
Feature: Login Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "Jane" is a Member with the account data
			| slug     | janedoe          |
			| email    | jane@example.com |
			| userName | JaneDoe          |
			| password | password3        |
		And she was able to start on the landing page

	@version:0.1.0
	Scenario: Navigate to login from landing page
		When "Jane" attempts to navigate to the login page
		Then she should see that they are on the login page
		And the login form is available

	@version:0.1.0
	Scenario: Sign in with valid member account
		Given that "Jane" was able to navigate to the login page
		When she attempts to login with the credentials
		Then she should see that they are on thier own member home page

	@version:0.1.0
	Scenario: Sign in with invalid credentials
		Given that "Jane" was able to navigate to the login page
		When she attempts to login with the credentials
			| email    | jane@example.com |
			| password | wrongpassword    |
		Then she should see the form validation error "Invalid user name or password"
