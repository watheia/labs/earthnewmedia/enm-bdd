@client_spa
@registration_page
@release:v0.1a
Feature: Registration Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "Barbra" is a is a Guest without an account
		And she was able to start on the landing page

	@version:0.1.0
	Scenario: Navigate to registration page from landing page
		When "Barbra" attempts to navigate to the registration page
		Then she should see that they are on the registration page
		And the registration card is available

	@version:0.1.0
	Scenario: Register new member account (happy path)
		Given that "Barbra" was able to navigate to the registration page
		When she attempts to register an account with
			| email     | barbra@example.com |
			| userName  | Barbra             |
			| password1 | password3          |
			| password2 | password3          |
		Then she should see that they are on thier own member home page

	@version:0.1.0
	Scenario: Register new member account (password mismatch)
		Given that "Barbra" was able to navigate to the registration page
		When she attempts to enter "barbra@example.com" into the registration form email field
		And "Barbra" into the registration form user name field
		And "password3" into the first registration form password field
		And "oops" into the second registration form password field
		Then she should see the submit member registration button is disabled

	@version:0.1.1
	Scenario: Register new member account (invalid email)
		Given that "Barbra" was able to navigate to the registration page

	@version:0.1.1
	Scenario: Register new member account (invalid user name)
		Given that "Barbra" was able to navigate to the registration page