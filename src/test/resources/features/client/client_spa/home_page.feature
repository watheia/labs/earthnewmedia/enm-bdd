@client_spa
@home_page
@release:v0.1a
Feature: Home Page

	Background:
		Given that an admin was able to initialize the system under test
		And that "John" is a Member with the account data
			| slug     | johndoe          |
			| email    | john@example.com |
			| userName | JohnDoe          |
			| password | password2        |
		And he was able to start on the login page

	@version:0.1.0
	Scenario: Redirect to member home page after login
		When "John" attempts to login to the spa
		Then he should see that they are on thier own member home page
		And the principal user widget is available
		And the search widget is available
		And the home nav is available
		And the account nav is available
		And the profile nav is available
		And the logout nav is available
		And the avatar image is available
		And the principal user name is "JohnDoe"
		And the create new library widget is available
		And the member library list is empty
