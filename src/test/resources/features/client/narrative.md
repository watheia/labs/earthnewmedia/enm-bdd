@release:v0.1a
enm-client

The initial version of the earthnew.media client is a simple React/Redux SPA with client-side routing (EG. HashRouter). Later versions of the client will provide proper pre-generation of content for better caching and CDN support.
