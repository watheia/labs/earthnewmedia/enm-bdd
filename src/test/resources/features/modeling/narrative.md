@release:v0.1b
enm-modeling

The earthnewmedia modeling projects provide a single source of truth for Domain Model entity relationships. Later versions will be able to leverage ECore/EMF advanced features for automatic code generation of api endpoints and form editors.
