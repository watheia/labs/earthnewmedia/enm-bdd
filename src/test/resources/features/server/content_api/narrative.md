@release:v0.1a
Content Api

The Content API is responsible for general Content operations outside the context of a specific Library, such as searching or administrative functions.

**Endpoints**

- GET `/content/{contentSlug}` Get Content Details
- GET `/content/count` Count Content
- GET `/content` Find Content
- POST `/content/{contentId}` Create Comment Thread
- PATCH `/content/{contentId}/{commentId}` Update Comment Thread