@release:v0.1a
Confirmation Api

The Confirmation API is responsible for wrapping potentially
destrictive actions into a storable action with a set expire
time. If the action is not confirmed before the experation
then the action should be destroyed
