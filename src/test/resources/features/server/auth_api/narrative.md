@release:v0.1a
Auth API

The Auth Api is responsible for validating user credentials and mannaging login sessions. Currently only JWT security is supported, but other protocols will be added in the future.

Example HTTP Header: `{"Authorization": "Bearer SECURE_TOKEN"}`

**Endpoints**

- POST `/auth/login` (login to api)
- GET`/auth/refresh` (refresh auth token)