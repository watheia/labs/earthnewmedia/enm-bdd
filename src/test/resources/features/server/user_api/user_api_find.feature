@server
@user_api
@release:v0.1a
Feature: Find Users API Endpoint

    The `GET /user` enpoint is used to search for `User` entities based on a
    set of dynamicly specified conditions.

    Background: 
        Given that an admin was able to initialize the system under test
        And that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | password | password2        |
        And he was able to login to the api

    @version:0.1.0
    Scenario: Use api to find all users
        When "John" attempts to use the api to find all users
        Then he should see that they recieved a valid UserCollection response
        And the response body has all of the entries matching
            | slug          | userName      | email             |
            | administrator | Administrator | admin@example.com |
            | john-doe      | John Doe      | john@example.com  |
            | jane-doe      | Jane Doe      | jane@example.com  |

    @version:0.1.0
    Scenario: Use api to find users (pagination)
        Given that "John" is able to use the api to find users
        And the api request limit is 2
        When he submits the api request
        Then he should see that they recieved a valid UserCollection response
        And the response body contains 2 entries
        
