@server
@user_api
@release:v0.1a
Feature: Get User API Endpoint

        The `GET ​/user​/{userSlug}` enpoint is retrieve a user by the slug alias

    @version:0.1.0
    Scenario: Use api to get user
        Given that an admin was able to initialize the system under test
        And that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | password | password2        |
        And he was able to login to the api
        And that "Jane" is a Member with the account data
            | slug     | jane-doe         |
            | email    | jane@example.com |
            | password | password3        |
        And she was able to login to the api
        When "Jane" attempts to use the api to get the user named "john-doe"
        Then she should see that they recieved a valid User response
        And the response body has an "email" equal to "john@example.com"
        And the response body has an "userName" equal to "John Doe"
        And the response body has a "slug" equal to "john-doe"
        And the response body has a "roles" containing "Member"
