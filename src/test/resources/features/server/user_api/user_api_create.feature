@server
@user_api
@release:v0.1a
Feature: Create User API Endpoint

    The `POST /user` enpoint is used to create a new `User` entity. A valid `LoginSession`
    is returned upon user creation

    # @version:0.1.0
    Scenario: Use api to create user
        Given that "Barbra" is a Guest without an account
        When she attempts to use the api to register a new member account with
            | email    | barbra@example.com |
            | userName | Barbra             |
            | password | password           |
        Then she should see that they recieved a valid LoginSession response
        # When "Barbra" attempts to use the api to get the principal
        # Then she should see that they recieved a valid User response
        # And the response body has an "email" equal to "barbra@example.com"
        # And the response body has an "userName" equal to "Barbra"
        # And the response body has a "slug" equal to "barbra"
        # And the response body has a "roles" containing "Member"
