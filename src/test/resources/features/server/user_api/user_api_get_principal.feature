@server
@user_api
@release:v0.1a
Feature: Get Principal API Endpoint

    The `GET ​/user​/me` endpoint returns the `User` entity tied to the
    current login session

    @version:0.1.0
    Scenario: Use api to get principal
        Given that an admin was able to initialize the system under test
        And that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | password | password2        |
        And he was able to login to the api
        When he attempts to use the api to get the principal
        Then he should see that they recieved a valid User response
        And the response body has an "email" equal to "john@example.com"
        And the response body has an "userName" equal to "John Doe"
        And the response body has a "slug" equal to "john-doe"
        And the response body has a "roles" containing "Member"
        And the response body has an "attributes" which is null
        And the response body has a "libraries" which is null