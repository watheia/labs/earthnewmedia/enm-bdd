@server
@user_api
@release:v0.1a
Feature: Count Users API Endpoint

    The `GET /user/count` enpoint may be used for pagination of search results

    Background:
        Given that an admin was able to initialize the system under test
        And that "Jane" is a Member with the account data
            | slug     | jane-doe         |
            | email    | jane@example.com |
            | password | password3        |
        And she was able to login to the api

    @version:0.1.0
    Scenario: Use api to count users
        When "Jane" attempts to use the api to count the users
        Then she should see that they recieved a valid Count response
        And the response body has a "count" equal to 3

    @version:0.1.0
    Scenario: Use api to count users (where userName eq)
        When "Jane" attempts to use the api to count the users with the query "where[slug]=john-doe"
        Then she should see that they recieved a valid Count response
        And the response body has a "count" equal to 1