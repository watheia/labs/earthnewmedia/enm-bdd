@server
@attribute_api
@release:v0.1a
Feature: User Api

    The `DELETE /user-attribute/{attributeId}` enpoint may be used to
    delete an existing `UserAttribute` which belongs to the principal `User`

    @version:0.1.0
    Scenario: Use api to delete attribute
        Given  that an admin was able to initialize the system under test
        And  that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | userName | John Doe         |
            | password | password2        |
        And  he was able to login to the api
        And  he was able to use the api to create an attribute called "Foo" with the content
            """
            [waweb.io](https://www.waweb.io)
            """
        When  she attempts to use the api to delte the attribute
        Then  she should see that they recieved a valid Count response
        And  the response body has a "count" equal to 1
