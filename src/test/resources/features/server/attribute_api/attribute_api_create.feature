@server
@attribute_api
@release:v0.1a
Feature: Create User Attribute API Endpoint

    The `POST /user-attribute` enpoint may be used to
    delete an existing `UserAttribute` which belongs to the principal `User`

    @version:0.1.0
    Scenario: Use api to create attribute
        Given that an admin was able to initialize the system under test
        And that "Jane" is a Member with the account data
            | slug     | jane-doe         |
            | email    | jane@example.com |
            | password | password3        |
        And she was able to login to the api
        And that "John" is a Member with the account data
            | slug     | john-doe         |
            | email    | john@example.com |
            | password | password2        |
        Given "John" was able to login to the api
        When he attempts to use the api to create an attribute called "Foo" with the content
            """
            [waweb.io](https://www.waweb.io)
            """
        Then he should see that they recieved a valid UserAttribute response
        And the response body has an "label" equal to "Foo"
        And the response body has an "content" equal to "[waweb.io](https://www.waweb.io)"
