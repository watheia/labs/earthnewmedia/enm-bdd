@server
@admin_api
@release:v0.1a
Feature: Reset Test Fixtures API Endpoint

   @version:0.1.0
   Scenario: Reset the test fixtures
      Given that "Felix" is an Admin with the account data
         | email    | admin@example.com |
         | password | password1         |
      And he was able to login to the api
      When "Felix" attepmts to use the api to reset the test fixtures
      Then he should see that they recieved a NoContent status code
