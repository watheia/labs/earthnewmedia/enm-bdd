@server
@admin_api
@release:v0.1a
Feature: Ping Api

      The Ping Api is a basic "Hello, World!" response without requiring any authentication

      **Endpoints**

      - GET `/ping` (get ping response)

   @version:0.1.0
   Scenario: Ping the api
      Given that "Bob" is a is a Guest without an account
      When he attempts to ping the api
      Then he should see that they recieved a valid Greeting response
