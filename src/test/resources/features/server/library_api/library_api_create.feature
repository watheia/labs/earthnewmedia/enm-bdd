@server
@library_api
@release:v0.1a
Feature: Create User Library API Endpoint

        The `POST /user-library` enpoint is used to delete a
        `UserLibrary` entity which belongs to the principal `User`

    @version:0.1.0
    Scenario: Use api to create libray
        Given that an admin was able to initialize the system under test
        And that "Jane" is a Member with the account data
            | slug     | jane-doe         |
            | email    | jane@example.com |
            | userName | Jane Doe         |
            | password | password3        |
        And she was able to login to the api
        When she attempts to use the api to create a library with
            | access      | Public         |
            | libraryType | Discussion     |
            | name        | My Discussions |
            | description | My Description |
        Then she should see that they recieved a valid UserLibrary response
