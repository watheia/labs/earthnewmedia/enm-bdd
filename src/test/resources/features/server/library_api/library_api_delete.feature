@server
@library_api
@release:v0.1a
Feature: Delete User Library API Endpoint

        The `DELETE /user-library/{libraryId}` enpoint is used to delete a
        `UserLibrary` entity which belongs to the principal `User`

    @version:0.1.0
    Scenario: Use api to delete libray
        Given that an admin was able to initialize the system under test
        And that "Jane" is a Member with the account data
            | slug     | jane-doe         |
            | email    | jane@example.com |
            | userName | Jane Doe         |
            | password | password3        |
        And she was able to login to the api
        And she was able to use the api to create a library with
            | access      | Public         |
            | libraryType | Discussion     |
            | name        | My Discussions |
            | description | My Description |
        When she attempts to use the api to delte the library
        Then she should see that they recieved a valid Count response
        And the response body has a "count" equal to 1