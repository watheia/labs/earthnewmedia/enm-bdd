/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions

import static earthnewmedia.bdd.api.UseTheAdminApi.resetTheTestFixtures

import earthnewmedia.bdd.api.tasks.LoginToApi
import earthnewmedia.bdd.common.Authenticate
import earthnewmedia.bdd.common.TheActor
import earthnewmedia.bdd.common.model.User
import earthnewmedia.bdd.common.model.UserRole
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CommonStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given("that an admin was able to initialize the system under test")
    void that_an_admin_was_able_to_initialize_the_system_under_test() {
        def user = new User(userName: "Administrator", email: "admin@example.com", password: 'password1')

        theActor.whoIsNamed(user.userName)
                .whoCan(Authenticate.asPrincipal(user))
                .whoCanUseTheApi()
                .wasAbleTo(LoginToApi.asPrincipal())
                .wasAbleTo(resetTheTestFixtures())
    }

    @Given("that {string} is a Guest without an account")
    void that_actor_is_a_is_a_guest_without_an_account(String actor) {
        theActor.whoIsNamed(actor)
                .whoCanUseTheApi()
    }

    @Given(/^that "([^"]*)" is an? (Admin|Member) with the account data$/)
    void that_is_actor_with_the_account_data(String actor, UserRole role, Map<String, String> principalData) {
      theActor.whoIsNamed(actor)
                .whoCan(Authenticate.asPrincipal(new User(principalData)))
                .whoCanUseTheApi()
    }
}
