/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.server.rest_api

import earthnewmedia.bdd.api.tasks.LoginToApi
import earthnewmedia.bdd.common.Authenticate
import earthnewmedia.bdd.common.TheActor
import earthnewmedia.bdd.common.model.User
import io.cucumber.java.Before
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class AuthApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @When("{string} attempts to login to the api")
    void attempts_to_login_to_the_api(String actor) {
        theActor.whoIsNamed(actor).attemptsTo LoginToApi.asPrincipal()
    }

    @When("{string} attempts to login to the api with the credentials")
    void attempts_to_login_to_the_api_with_the_credentials(String actor, Map<String, String> credentials) {
        theActor.whoIsNamed(actor)
                .whoCan(Authenticate.asPrincipal(new User(credentials)))
                .attemptsTo(LoginToApi.asPrincipal())
    }
}
