/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.server.rest_api

import static earthnewmedia.bdd.api.UseTheUserApi.*
import static earthnewmedia.bdd.api.MakeApiRequest.request

import earthnewmedia.bdd.common.TheActor
import earthnewmedia.bdd.api.filter.WhereClause
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps
import net.thucydides.core.annotations.Shared

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
public class UserApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given("that {string} is able to use the api to find users")
    void is_able_to_use_the_api_to_find_users_using_a_filter(String actor) {
        theActor.whoIsNamed(actor).whoCan request(findTheUsers())
    }

    @When("{string} attempts to use the api to get the principal")
    attempts_to_use_the_api_to_get_the_principal(String actor) {
        theActor.whoIsNamed(actor).attemptsTo getThePrincipal()
    }

    @When(/s?he attempts to use the api to get the principal$/)
    void attempts_to_use_the_api_to_get_the_principal() {
        theActor.attemptsTo getThePrincipal()
    }

     @When("{string} attempts to use the api to get the user named {string}")
    void attempts_to_use_the_api_to_get_the_user(String actor, String userSlug) {
        theActor.whoIsNamed(actor).attemptsTo getTheUser(userSlug)
    }

    @When(/^s?he attempts to use the api to get the user named "([^"]*)"$/)
    void attempts_to_use_the_api_to_get_the_user(String userSlug) {
        theActor.attemptsTo getTheUser(userSlug)
    }

    @When("{string} attempts to use the api to register a new member account with")
    void attempts_to_use_the_api_to_register_a_new_member_account_with(String actor,
            Map<String, String> userData) {
        theActor.whoIsNamed(actor).attemptsTo createTheUser(userData)
    }

    @When(/s?he attempts to use the api to register a new member account with$/)
    void attempts_to_use_the_api_to_register_a_new_member_account_with(Map<String, String> userData) {
        theActor.attemptsTo createTheUser(userData)
    }

    @When("{string} attempts to use the api to find all users")
    void attempts_to_use_the_api_to_find_all_users(String actor) {
        theActor.whoIsNamed(actor).attemptsTo findTheUsers()
    }

    @When(/^s?he attempts to use the api to find all users$/)
    void attempts_to_use_the_api_to_find_all_users() {
        theActor.attemptsTo findTheUsers()
    }

    @When("{string} attempts to use the api to count the users")
    void attempts_to_use_the_api_to_count_the_users(String actor) {
        theActor.whoIsNamed(actor).attemptsTo countTheUsers()
    }

    @When(/^s?he attempts to use the api to count the users$/)
    void attempts_to_use_the_api_to_count_the_users() {
        theActor.attemptsTo countTheUsers()
    }

    @When("{string} attempts to use the api to count the users with the query {string}")
    void attempts_to_use_the_api_to_count_the_users_with_the_query(String actor, String query) {
        theActor.whoIsNamed(actor).attemptsTo countTheUsers().withTheQuery(query)
    }

    @When(/^s?he attempts to use the api to count the users with the query "([^"]*)"$/)
    void attempts_to_use_the_api_to_count_the_users_with_the_query(String query) {
        theActor.attemptsTo countTheUsers().withTheQuery(query)
    }

    // // User Attributes
    // ////

    // @Given(/^s?he was able to use the api to create an attribute called "([^"]*)" with the content$/)
    // void was_able_to_use_the_api_to_create_an_attribute_called_with_the_content(String label, String content) {
    //     def userId = theActor.asPrincipal().id
    //     theActor.wasAbleTo createTheAttribute(userId, [label: label, content: content])
    // }

    // @When(/^s?he attempts to use the api to create an attribute called "([^"]*)" with the content$/)
    // void attempts_to_use_the_api_to_create_an_attribute_called_with_the_content(String label, String content) {
    //     def userId = theActor.asPrincipal().id
    //     theActor.attemptsTo createTheAttribute(userId, [label: label, content: content])
    // }

    // @When(/^s?he attempts to use the api to update the attribute with$/)
    // void attempts_to_use_the_api_to_update_the_attribute_with(Map<String, String> atributeData) {
    //     def userId = theActor.asPrincipal().id
    //     def attributeId = theActor.lastResponse().jsonPath().getString('id')
    //     theActor.attemptsTo updateTheAttribute(userId, attributeId, atributeData)
    // }

    // @When(/^s?he attempts to use the api to delte the attribute$/)
    // void attempts_to_use_the_api_to_delte_the_attribute() {
    //     def userId = theActor.asPrincipal().id
    //     def attributeId = theActor.lastResponse().jsonPath().getString('id')
    //     theActor.attemptsTo deleteTheAttribute(userId, attributeId)
    // }

    // // User Libraries
    // ////

    // @Given(/^s?he was able to use the api to create a library with$/)
    // void was_able_to_use_the_api_to_create_a_library_with(Map<String, String> libraryData) {
    //     def userId = theActor.asPrincipal().id
    //     theActor.wasAbleTo createTheLibrary(userId, libraryData)
    // }

    // @When(/^s?he attempts to use the api to create a library with$/)
    // void attempts_to_use_the_api_to_create_a_library_with(Map<String, String> libraryData) {
    //     def userId = theActor.asPrincipal().id
    //     theActor.attemptsTo createTheLibrary(userId, libraryData)
    // }

    // @When(/s?he attempts to use the api to update the library with$/)
    // void attempts_to_use_the_api_to_update_the_library_with(Map<String, String> libraryData) {
    //     def userId = theActor.asPrincipal().id
    //     def libraryId = theActor.lastResponse().jsonPath().getString('id')
    //     theActor.attemptsTo updateTheLibrary(userId, libraryId, libraryData)
    // }

    // @When(/^s?he attempts to use the api to delte the library$/)
    // void attempts_to_use_the_api_to_delte_the_library() {
    //     def userId = theActor.asPrincipal().id
    //     def libraryId = theActor.lastResponse().jsonPath().getString('id')
    //     theActor.attemptsTo deleteTheLibrary(userId, libraryId)
    // }
}
