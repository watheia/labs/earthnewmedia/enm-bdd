/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.server.rest_api

import static earthnewmedia.bdd.api.questions.ResponseBody.theResponseBody
import static earthnewmedia.bdd.api.questions.ResponseStatus.theResponseStatus
import static earthnewmedia.bdd.common.Matchers.*
import static earthnewmedia.bdd.api.MakeApiRequest.request
import static earthnewmedia.bdd.api.MakeApiRequest.lastRequestOf
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse
import static org.hamcrest.Matchers.*

import earthnewmedia.bdd.api.MakeApiRequest
import earthnewmedia.bdd.api.ApiSchema
import earthnewmedia.bdd.api.StatusCode
import earthnewmedia.bdd.api.tasks.LoginToApi
import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.When
import io.cucumber.java.en.Then
import io.cucumber.datatable.DataTable
import io.restassured.response.ValidatableResponse
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
public class CommonApiStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given("{string} was able to login to the api")
    void was_able_to_login_to_the_api(String actor) {
        theActor.whoIsNamed(actor).wasAbleTo LoginToApi.asPrincipal()
    }

    @Given(/^s?he was able to login to the api$/)
    void was_able_to_login_to_the_api() {
        theActor.wasAbleTo LoginToApi.asPrincipal()
    }

    @When(/^s?he submits the api request$/)
    void submits_the_api_request() {
        def submitRequest = lastRequestOf(theActor.inTheSpotlight()).build()
        theActor.attemptsTo submitRequest
    }

    @Then(/^the response body has an? "([^"]*)" equal to "([^"]*)"$/)
    void the_response_body_has_field_equal_to(String field, String expectedVal) {
        theActor.shouldSeeThat theResponseBody({ it.get(field) }),
                equalTo(expectedVal)
    }

    @Then(/^the response body has an? "([^"]*)" equal to (\d+)$/)
    void the_response_body_has_field_equal_to(String field, Integer expectedVal) {
        theActor.shouldSeeThat theResponseBody({ it.get(field) }),
                equalTo(expectedVal)
    }

    @Then(/^the response body has an? "([^"]*)" containing "([^"]*)"$/)
    void the_response_body_has_field_containing(String field, String expectedVal) {
        theActor.shouldSeeThat theResponseBody({ it.get(field) }),
                contains(expectedVal)
    }

    @Then(/^the response body has an? "([^"]*)" which is null$/)
    void the_response_body_has_field_which_is_null(String field) {
        theActor.shouldSeeThat theResponseBody({ it.get(field) }),
                is(null)
    }

    @Then(/^the response body has an? "([^"]*)" with a user id equal to "([^"]*)"$/)
    void the_response_body_has_a_field_with_a_user_id_equal_to(String field, String actor) {
        def principal = theActor.whoIsNamed(actor).asPrincipal()
        theActor.shouldSeeThat theResponseBody({ it.get(field) }),
                equalTo(principal.id)
    }

    @Then(/^the response body contains (\d+) (entry|entries)$/)
    void the_response_body_contains_entries(Integer expectedSize, String x) {
        theActor.shouldSeeThat theResponseBody(),
                hasSize(expectedSize)
    }

    @Then("the response body has all of the entries matching")
    void the_response_body_has_all_of_the_entries_matching(DataTable expectedEntries) {
        theActor.shouldSeeThat theResponseBody(),
                hasItemsMatching(expectedEntries.asMaps(String, String))
    }

    @Then(/^s?he should see that they recieved a valid (Count|Greeting|LoginSession|User|UserCollection|UserAttribute|UserLibrary) response$/)
    void should_see_that_they_recieved_a_valid_response(ApiSchema schema) {
        theActor.should seeThatResponse({ ValidatableResponse response ->
            schema.toFile().withReader  { Reader reader ->
                response.body(matchesJsonSchema(reader))
            }
        })
    }

    @Then(/^s?he should see that they recieved an? (Ok|NoContent|Unauthorized) status code$/)
    void should_see_that_they_recieved_a_status_code(StatusCode expectedStatus) {
        theActor.shouldSeeThat theResponseStatus(),
                equalTo(expectedStatus.code)
    }

    @Given("the api request limit is {int}")
    void the_api_request_limit_is(Integer limit) {
        lastRequestOf(theActor.inTheSpotlight()).limit(limit)
    }
}
