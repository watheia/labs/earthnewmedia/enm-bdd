package earthnewmedia.bdd.stepdefinitions.client.client_spa

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

class CommonSpaStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }

    @Given("that {string} was able to start on the landing page")
    void was_able_to_start_on_the_landing_page(String actor) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Given(/^s?he was able to start on the landing page$/)
    void was_able_to_start_on_the_landing_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Given(/s?he was able to start on thier own member home page$/)
    void was_able_to_start_on_thier_own_member_home_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Given("she was able to navigate to thier own member home page")
    void was_able_to_navigate_to_thier_own_member_home_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }
    
    @Given("that {string} was able to start on thier own member account page")
    void was_able_to_start_on_thier_own_member_account_page(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }
    
    @Given("he was able to start on the login page")
    void was_able_to_start_on_the_login_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @When("she refreshes the page")
    void refreshes_the_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("she should see the form validation error {string}")
    void should_see_the_form_validation_error(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }
}
