/**
 * 
 */
package earthnewmedia.bdd.stepdefinitions.client.client_spa

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.Before
import io.cucumber.java.en.Then
import net.serenitybdd.screenplay.actors.OnStage
import net.serenitybdd.screenplay.actors.OnlineCast
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class HomePageStepDefinitions {

    @Steps
    TheActor theActor

    @Before
    void set_the_stage(){
        OnStage.setTheStage(new OnlineCast())
    }


    @Then(/^s?he should see that they are on thier own member home page$/)
    void should_see_that_they_are_on_thier_own_member_home_page() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the principal user widget is available")
    void the_principal_user_widget_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the principal user name is {string}")
    void the_principal_user_name_is(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the account nav is available")
    void the_account_nav_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the profile nav is available")
    void the_profile_nav_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the logout nav is available")
    void the_logout_nav_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }

    @Then("the avatar image is available")
    void the_avatar_image_is_available() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException()
    }
}
