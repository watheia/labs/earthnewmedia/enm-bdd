/**
 * 
 */
package earthnewmedia.bdd.api

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.PendingException
import net.thucydides.core.annotations.Step
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UseTheLibraryApi {

    String actor

    @Steps
    TheActor theActor


    @Step("#actor uses the api to count the libraries")
    void toCountLibrary() {
        throw new PendingException("TODO: not implemented")
    }

    @Step("#actor uses the api to find the libraries")
    void toFindLibrary() {
        throw new PendingException("TODO: not implemented")
    }

    @Step("#actor uses the api to get the library details")
    void toGetLibraryDetails() {
        throw new PendingException("TODO: not implemented")
    }
    
    // Content
    ////
    
    @Step("#actor uses the api to create content")
    void toCreateContent() {
        throw new PendingException("TODO: not implemented")
    }
    
    @Step("#actor uses the api to update content")
    void toUpdateContent() {
        throw new PendingException("TODO: not implemented")
    }
    
    @Step("#actor uses the api to delete content")
    void toDeleteContent() {
        throw new PendingException("TODO: not implemented")
    }
 
}
