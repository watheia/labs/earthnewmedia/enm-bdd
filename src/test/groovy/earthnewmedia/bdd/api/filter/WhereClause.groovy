/**
 * 
 */
package earthnewmedia.bdd.api.filter

import groovy.transform.Canonical

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
@Canonical
class WhereClause {
    String key
    WhereOp op
    String value
}
