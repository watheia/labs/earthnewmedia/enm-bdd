/**
 * 
 */
package earthnewmedia.bdd.api

import earthnewmedia.bdd.api.tasks.ApiTask
import earthnewmedia.bdd.common.TheActor

import net.thucydides.core.annotations.Step
import net.thucydides.core.annotations.Steps
import net.serenitybdd.screenplay.Ability
import net.serenitybdd.screenplay.Actor

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
class MakeApiRequest implements Ability {

    final ApiTask request

    Integer limit = null

    static MakeApiRequest request(ApiTask request) {
        return new MakeApiRequest(request)
    }

    static MakeApiRequest lastRequestOf(Actor actor) {
        return actor.abilityTo(MakeApiRequest)
    }

    MakeApiRequest limit(int limit) {
        this.limit = limit
        return this
    }

    ApiTask build() {
        return (limit == null)? 
            request : request.withFilter([ limit: limit ])
    }

    private MakeApiRequest(ApiTask request) {
        this.request = request
    }
}