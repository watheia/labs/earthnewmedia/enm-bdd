/**
 * 
 */
package earthnewmedia.bdd.api

import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.api.tasks.ApiGet
import earthnewmedia.bdd.api.tasks.ApiTask
import io.cucumber.java.PendingException

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UseThePingApi {
    public static ApiTask pingTheApi() {
        return instrumented(ApiGet, Endpoint.Ping.ping())
    }
}
