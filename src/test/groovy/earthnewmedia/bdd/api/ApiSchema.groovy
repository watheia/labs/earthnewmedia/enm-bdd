package earthnewmedia.bdd.api

enum ApiSchema {
    Count("count"),
    Greeting("greeting"),
    LoginSession("login-session"),
    User("user"),
    UserCollection("user-collection"),
    UserAttribute("user-attribute"),
    UserLibrary("user-library")
    
    private final String _name
    
    String toPath() {
        return "schemas/${_name}.schema.json"
    }
    
    URI toURI() {
        return this.getClass().getResource(this.toPath()).toURI()
    }
    
    File toFile() {
        return new File(this.toURI())
    }
    
    private ApiSchema(String name) {
        _name = name
    }
}
