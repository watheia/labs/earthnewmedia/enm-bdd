/**
 * 
 */
package earthnewmedia.bdd.api

import static earthnewmedia.bdd.api.Endpoint.Admin.*
import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.api.tasks.*

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.PendingException
import net.thucydides.core.annotations.Step
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UseTheAdminApi {
    static ApiTask resetTheTestFixtures() {
        return instrumented(ApiGet, Endpoint.Admin.resetTestFixtures())
    }
}
