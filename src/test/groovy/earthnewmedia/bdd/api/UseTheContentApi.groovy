/**
 * 
 */
package earthnewmedia.bdd.api

import earthnewmedia.bdd.common.TheActor
import io.cucumber.java.PendingException
import net.thucydides.core.annotations.Step
import net.thucydides.core.annotations.Steps

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UseTheContentApi {

    String actor

    @Steps
    TheActor theActor


    @Step("#actor uses the api to count the content")
    void toCountContent() {
        throw new PendingException("TODO: not implemented")
    }

    @Step("#actor uses the api to find the content")
    void toFindContent() {
        throw new PendingException("TODO: not implemented")
    }

    @Step("#actor uses the api to get the content details")
    void toGetContentDetails() {
        throw new PendingException("TODO: not implemented")
    }
    
    // Comments
    ////
    
    @Step("#actor uses the api to create a top-level comment")
    void toCreateComment() {
        throw new PendingException("TODO: not implemented")
    }
    
    @Step("#actor uses the api to update a top-level comment")
    void toUpdateComment() {
        throw new PendingException("TODO: not implemented")
    }
 
}
