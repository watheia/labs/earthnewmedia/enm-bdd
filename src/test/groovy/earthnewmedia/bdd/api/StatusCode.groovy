/**
 * 
 */
package earthnewmedia.bdd.api

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
enum StatusCode {
    Ok(200),
    NoContent(204),
    Unauthorized(401)
    
    private final int code
    
    int getCode() {
        return this.code
    }
    
    private StatusCode(int code) {
        this.code = code
    }
}
