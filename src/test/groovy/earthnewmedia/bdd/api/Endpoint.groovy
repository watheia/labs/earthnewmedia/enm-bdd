/**
 * 
 */
package earthnewmedia.bdd.api

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class Endpoint {
    static String api() { "http://localhost:3000" }
    
    
    static class Admin {
        static String resetTestFixtures() { "/admin/reset-test-fixtures" }
        static String updateAllUsers() { "/admin/user" }
        static String replaceUser(String userId) { "/admin/user/${userId}" }
        static String deleteUser(String userId) { "/admin/user/${userId}" }
        static String updateAllComments() { "/admin/comment" }
        static String replaceComment(String commentId) { "/admin/comment/${commentId}" }
        static String deleteComment(String commentId) { "/admin/comment/${commentId}" }
        static String updateAllLibraries() { "/admin/library" }
        static String replaceLibrary(String libraryId) { "/admin/comment/${libraryId}" }
        static String updateAllContent() { "/admin/content" }
        static String replaceContent(String contentId) { "/admin/content/${contentId}" }
    }
    static class Auth {
        static String login() { "/auth/login" }
        static String refresh() { "/auth/refresh" }
    }

    static class Ping {
        static String ping() { "/ping" }
    }
    
    static class User {
        static String me() { "/user/me" }
        static String get(String userSlug) { "/user/${userSlug}" }
        static String create() { "/user" }
        static String find() { "/user" }
        static String count() { "/user/count" }
        static String createAttribute(String userId) { "/user/${userId}/attributes" }
        static String updateAttribute(String userId, String attributeId) { "/user/${userId}/attributes/${attributeId}" }
        static String deleteAttribute(String userId, String attributeId) { "/user/${userId}/attributes/${attributeId}" }
        static String createLibrary(String userId) { "/user/${userId}/libraries" }
        static String updateLibrary(String userId, String libraryIdId) { "/user/${userId}/libraries/${libraryIdId}" }
        static String deleteLibrary(String userId, String libraryIdId) { "/user/${userId}/libraries/${libraryIdId}" }
    }
}
