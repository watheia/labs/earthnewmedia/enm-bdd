/**
 * 
 */
package earthnewmedia.bdd.api

import static earthnewmedia.bdd.api.Endpoint.User.*
import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.api.tasks.*

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class UseTheUserApi {

    static ApiTask createTheUser(Map data) {
        return instrumented(ApiPost, create(), data)
    }

    static ApiTask countTheUsers() {
        return instrumented(ApiGet, count())
    }

    static ApiTask findTheUsers() {
        return instrumented(ApiGet, find())
    }
    
    static ApiTask getThePrincipal() {
        return instrumented(ApiGet, me())
    }

    static ApiTask getTheUser(String userSlug) {
        assert userSlug
        return instrumented(ApiGet, get(userSlug))
    }


    // Attributes
    ////

    static ApiTask createTheAttribute(String userId, Map data) {
        return instrumented(ApiPost, createAttribute(userId), data)
    }

    static ApiTask updateTheAttribute(String userId, String attributeId, Map data) {
        return instrumented(ApiPatch, updateAttribute(userId, attributeId), data)
    }

    static ApiTask deleteTheAttribute(String userId, String attributeId) {
         return instrumented(ApiDelete, deleteAttribute(userId, attributeId))
    }

    // Libraries
    ////

    static ApiTask createTheLibrary(String userId, Map data) {
        return instrumented(ApiPost, createLibrary(userId), data)
    }

    static ApiTask updateTheLibrary(String userId, String libraryId, Map data) {
        return instrumented(ApiPatch, updateLibrary(userId, libraryId), data)
    }

    static ApiTask deleteTheLibrary(String userId, String libraryId) {
         return instrumented(ApiDelete, deleteLibrary(userId, libraryId))
    }
}
