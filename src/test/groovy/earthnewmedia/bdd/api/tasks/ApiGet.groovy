/**
 * 
 */
package earthnewmedia.bdd.api.tasks

import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.rest.interactions.Get
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class ApiGet extends ApiTask {

    @Step("{0} attempts to GET from #resource")
    <T extends Actor> void performAs(T actor) {
        assert resource
        actor.attemptsTo Get.resource(getEndpointUri()).withRequest({ RequestSpecification req ->
            prepareRequest(actor, req)
        })
    }

    ApiGet(String resource) {
        this.resource = resource
    }
}
