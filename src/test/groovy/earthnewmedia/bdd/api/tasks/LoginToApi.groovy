/**
 * 
 */
package earthnewmedia.bdd.api.tasks

import static earthnewmedia.bdd.api.UseTheAuthApi.loginToApi
import static net.serenitybdd.screenplay.Tasks.instrumented

import earthnewmedia.bdd.common.Authenticate
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class LoginToApi implements Task {

    static LoginToApi asPrincipal() {
        return instrumented(LoginToApi)
    }

    @Step("{0} logs into api as the principal")
    <T extends Actor> void performAs(T actor) {
        def principal = Authenticate.asPrincipal(actor)
        actor.attemptsTo loginToApi().withPayload([email: principal.email, password: principal.password])
        
        // Update the Authenticate ability with the login session info
        def lastResponse = LastResponse.received().answeredBy(actor)
        principal.id = lastResponse.jsonPath().getString('userId')
        principal.token = lastResponse.jsonPath().getString('token')
    }
}
