/**
 * 
 */
package earthnewmedia.bdd.api.tasks

import static java.net.URLEncoder.encode

import earthnewmedia.bdd.common.Authenticate
import earthnewmedia.bdd.common.CannotAuthenticateException
import earthnewmedia.bdd.api.filter.FilterBuilder
import earthnewmedia.bdd.api.filter.WhereClause
import groovy.json.JsonOutput
import io.restassured.specification.RequestSpecification
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Task

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
abstract class ApiTask implements Task {

    Authenticate principal

    String resource

    Object payload

    Map filter = null
    
    String query = null

    ApiTask withPayload(Map payload) {
        this.payload = payload
        return this
    }
    
    ApiTask withTheQuery(String query) {
        this.query = query
        return this
    }

    ApiTask withFilter(Map filter) {
        this.filter = filter
        return this
    }

    String getEndpointUri() {
        return (query == null)? resource : "${resource}?${query}"
    }

    public <T extends Actor> RequestSpecification prepareRequest(final T actor, final RequestSpecification req) {
        req.header("Content-Type", "application/json")
        if(filter) {
            assert query == null, "May not use filter when query string in use"
            req.queryParam('filter', filter)
        }
        if(!principal) {
            try {
                principal = Authenticate.asPrincipal(actor)
            }
            catch(CannotAuthenticateException e) {
                // swallow error
            }
        }

        if(principal && principal.token) {
            req.header("Authorization", "Bearer ${principal.token}")
        }

        return req
    }
}
