/**
 * 
 */
package earthnewmedia.bdd.common

import static net.serenitybdd.screenplay.EventualConsequence.eventually
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight

import java.util.function.Predicate

import org.hamcrest.Matcher

import earthnewmedia.bdd.api.Endpoint
import earthnewmedia.bdd.common.model.User
import earthnewmedia.bdd.common.model.UserRole
import io.restassured.response.Response
import net.serenitybdd.screenplay.Ability
import net.serenitybdd.screenplay.Actor
import net.serenitybdd.screenplay.Consequence
import net.serenitybdd.screenplay.Performable
import net.serenitybdd.screenplay.Question
import net.serenitybdd.screenplay.rest.abilities.CallAnApi
import net.serenitybdd.screenplay.rest.questions.LastResponse
import net.thucydides.core.annotations.Step

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class TheActor {

    String actor

    Actor inTheSpotlight() {
        return theActorInTheSpotlight()
    }

    Authenticate asPrincipal(String actor = null) {
        return actor? Authenticate.asPrincipal(theActorCalled(actor)) :
                Authenticate.asPrincipal(theActorInTheSpotlight())
    }

    Response lastResponse(String actor = null) {
        return actor? LastResponse.received().answeredBy(theActorCalled(actor)) :
                LastResponse.received().answeredBy(theActorInTheSpotlight())
    }

    @Step("{0} is an actor using the system under test")
    TheActor whoIsNamed(String actor) {
        theActorCalled(actor)
        return this
    }

    @Step("#actor is a Guest")
    TheActor whoIsAGuest() {
        return this
    }

    @Step("#actor is a {0}")
    TheActor whoIsA(UserRole role) {
        return this
    }

    @Step("#actor has an account with {0}")
    TheActor whoHasTheAccount(Map accountData) {
        return this
    }

    @Step("#actor can authenticate as {0}")
    TheActor whoCanAuthenticateAs(User principal) {
        theActorInTheSpotlight()
                .whoCan(Authenticate.asPrincipal(principal))

        return this
    }

    @Step("#actor can call the rest api")
    TheActor whoCanUseTheApi() {
        theActorInTheSpotlight()
                .whoCan(CallAnApi.at(Endpoint.api()))

        return this
    }

    public <T extends Ability> TheActor abilityTo(T doSomething) {
        return theActorInTheSpotlight().abilityTo(doSomething)
    }

    public TheActor wasAbleTo(Performable... todos) {
        theActorInTheSpotlight().wasAbleTo(todos)

        return this
    }

    public TheActor attemptsTo(Performable... tasks) {
        theActorInTheSpotlight().attemptsTo(tasks)

        return this
    }

    public TheActor shouldEventually(Consequence... consequences) {
        consequences.each {
            theActorInTheSpotlight().should(eventually(it))
        }

        return this
    }

    public TheActor should(Consequence... consequences) {
        theActorInTheSpotlight().should(consequences)

        return this
    }

    public <T extends Ability> TheActor whoCan(T doSomething) {
        theActorInTheSpotlight().whoCan(doSomething)
        return this
    }

    public <T> TheActor shouldSeeThat(Question<? extends T> actual, Matcher<T> expected) {
        return this.should(seeThat(actual, expected))
    }

    public <T> TheActor shouldSeeThat(Question<? extends T> actual, Predicate<T> expected) {
        return this.should(seeThat(actual, expected))
    }

    public <T> TheActor shouldSeeThat(String subject, Question<? extends T> actual, Predicate<T> expected) {
        return this.should(seeThat(subject, actual, expected))
    }

    public <T> TheActor shouldSeeThat(String subject, Question<? extends T> actual, Matcher<T> expected) {
        return this.should(seeThat(subject, actual, expected))
    }

    public <T> TheActor shouldSeeThat(Question<Boolean> actual) {
        return this.should(seeThat(actual))
    }

    public <T> TheActor shouldSeeThat(String subject, Question<Boolean> actual) {
        return this.should(seeThat(subject, actual))
    }
}
