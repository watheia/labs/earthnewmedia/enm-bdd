package earthnewmedia.bdd.common.model

import groovy.transform.Canonical

@Canonical
class User {
    String id
    String token
    String slug
    String email
    String userName
    String password
}
