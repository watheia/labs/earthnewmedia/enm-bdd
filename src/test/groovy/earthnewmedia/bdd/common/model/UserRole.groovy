package earthnewmedia.bdd.common.model

enum UserRole {
    Admin, Support, Member, Guest
}
