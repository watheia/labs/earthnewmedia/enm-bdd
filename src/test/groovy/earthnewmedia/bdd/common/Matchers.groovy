/**
 * 
 */
package earthnewmedia.bdd.common

import static org.hamcrest.Matchers.*
import org.hamcrest.Matcher
import java.lang.Iterable



/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class Matchers {

    static <T> Matcher<Iterable<? extends T>> hasItemsMatching(List<T> items) {
        return containsInAnyOrder(items.collect { entry ->
            allOf entry.collect { k, v -> hasEntry(k, v) }
        })
    }
}