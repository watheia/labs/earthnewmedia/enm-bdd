/**
 * 
 */
package earthnewmedia.bdd.common

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class CannotAuthenticateException extends RuntimeException {
    CannotAuthenticateException(String actorName) {
        super("The actor " + actorName + " does not have the ability to sign into the application (system under test)")
    }
}
